/*

This is a wrapper for Julian Stirling's "libdict.scad" that
cleans up the namespace by defining only the public functions,
and making sure everything is properly prefixed.


This library add dictionary like features to openscad
the dictionary is a list of pairs as we cannot define new types.
Unlike a python dictionary we cannot do a proper hash table for speed
so everything is build around the OpenSCAD search function
There are a lot of assert statements to ensure data in the "dictionary"
is in the format of
* A list of lists
* Each internal list has a length of two
* First element of each internal list is a string (the key)
* All keys are unique.

*/

use <./libdict_private.scad>;

// This function returns true if the value is in the list
// value must be a string or a number.
function dict_is_in(value, list) = is_in(value, list);

// Returns true if all emements in list are unique.
function dict_is_unique(list) = is_unique(list);

// Return true if a dictionary is valid
function dict_valid(dict) = valid_dict(dict);

// Key lookup for key value pair "dictionary".
// Unlike the built in lookup this works with strings.
function dict_lookup(key, dict) = key_lookup(key, dict);

// Creates a new dictionary with a key value pair replaced. Pair must already
// be in dictionary.
function dict_replace_value(key, value, dict) = replace_value(key, value, dict);

// Creates a new dictionary with a a set of key value pair replaced
// both inputs must be a dictionary. All keys in input must already be
// in dictionary
function dict_replace_multiple_values(rep_dict, dict) = replace_multiple_values(rep_dict, dict);