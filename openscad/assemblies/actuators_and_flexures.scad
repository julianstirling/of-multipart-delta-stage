/* This file is part of the openflexure multipart delta stage

(c) 2019 Richard Bowman, released under CERN-OHL-S v2
*/

use <../libs/libofu.scad>;
use <../sub_parts/vertical_actuator.scad>;
use <../parts/base_and_actuators.scad>;
use <../parts/parallelogram_flexure.scad>;
use <../parts/moving_carriage.scad>;

flex_dz = 75;
flex_dy = 75/2.5;
xy_parallelogram_w = 50;
parallelogram_params = parallelogram_flexure_default_params();

for(a=[0,-120, 120]) rotate(a){
    translate([0,-flex_dy-xy_parallelogram_w/2,0]) {
        actuator_column_and_anchors(0);
        actuator_flexures();
        //%actuator_column_void();
        translate([0, -8, 0]) rotate([60, 0,0]){
            color("pink") parallelogram_flexure(parallelogram_params);
        }
    }
}

color("lightgreen") moving_carriage();

//rotate(180) actuator_column_and_anchors(0);