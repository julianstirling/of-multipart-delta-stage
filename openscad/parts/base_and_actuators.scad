/* This file is part of the openflexure multipart delta stage

(c) 2019 Richard Bowman, released under CERN Open Hardware License
*/

use <../libs/libofu.scad>;
use <../sub_parts/vertical_actuator.scad>;

flex_dz = 75;
flex_dy = 75/2.5;
xy_parallelogram_w = 50;

module base_and_actuators_justactuators(){
    for(a=[0,-120, 120]) rotate(a){
        translate([0,-flex_dy-xy_parallelogram_w/2,0]) {
            actuator_column_and_anchors(0);
            actuator_flexures();
            %actuator_column_void();
        }
    }
}

base_and_actuators_justactuators();
//rotate(180) actuator_column_and_anchors(0);