/* This file is part of the openflexure multipart delta stage

(c) 2019 Richard Bowman, released under CERN Open Hardware License
*/

use <../libs/libofu.scad>;

bottom = 75*sin(60) - 8;

module moving_carriage_inner_block(){
    translate([-15, 25 - 3, bottom]) cube([30, 6, 6]);
}

module moving_carriage(){
    difference(){
        union(){
            hull() for(a=[0, 120, -120]) rotate(a + 60){
                moving_carriage_inner_block();
            }
            translate([0, 0, bottom - 6]) ofu_sequential_hull(){
                translate([-14, 0, 0]) cube([28, 20, 6 + ofu_tiny()]);
                translate([-8, 35, 0]) cube([16, 2, 6 + ofu_tiny()]);
                translate([-15, 45, 0]) cube([30, 30, 6 + ofu_tiny()]);
            }
            
        }
        
        cylinder(d=30, h=999);
        translate([0,60,0]) cylinder(d=25, h=999);
    }
}

moving_carriage();